## Qogir Icon Theme
A flat colorful design icon theme for Qogir theme : https://github.com/vinceliuice/Qogir-theme

## Install

`./Install` 

## Screenshots
![1](https://github.com/vinceliuice/Qogir-icon-theme/blob/master/preview_01.png?raw=true)
![2](https://github.com/vinceliuice/Qogir-icon-theme/blob/master/preview_02.png?raw=true)
